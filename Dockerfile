FROM acoshift/go-alpine

RUN mkdir ./app_log

VOLUME app_log

ADD pinterest-api-server /

COPY postgres.yaml .

COPY locations.html .

CMD ./pinterest-api-server
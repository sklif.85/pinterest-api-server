package main

import (
	"fmt"
)

const (
	confPath = "postgres.yaml"
	logPath  = "app_log"
	logFile  = "logs"
)

func main() {

	zapLog := Logger(logPath, logFile)

	server := Server(zapLog, confPath)

	server.Router()

	if err := server.s.ListenAndServe(); err != nil {
		zapLog.Fatal(err.Error())
	} else {
		server.l.Info(fmt.Sprintf("Server START on 127.0.0.1:%s", ":8080"))
	}
}

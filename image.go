package main

import (
	"database/sql"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

type Pin struct {
	Pin DataType `json:"pin"`
}

type DataType struct {
	ImageID  string `json:"image_id"`
	Title    string `json:"title"`
	Link     string `json:"link"`
	ImageSrc string `json:"image_src"`
}

type Image struct {
	ID            string    `json:"id"`
	Name          string    `json:"name"`
	Description   string    `json:"description"`
	DatePublished string    `json:"date_published"`
	ContentURL    string    `json:"content_url"`
	ImageLink     string    `json:"image_link"`
	Keywords      string    `json:"keywords"`
	UpdatedAT     time.Time `json:"updated_at"`
}

func (s *ServerType) Image(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	s.l.Info(vars["pinid"])

	data, err := s.Resolver(vars["pinid"])
	if err != nil {
		s.responseWriter(w, err.Error())
		return
	}

	s.responseWriter(w, data)
}

func (s *ServerType) responseWriter(w http.ResponseWriter, v interface{}) {
	payload, err := json.Marshal(v)
	if err != nil {
		s.l.Error("Marshal")
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(payload)
	s.l.Info("payload success")
}

func (s *ServerType) Resolver(pinID string) (pin Pin, err error) {

	pin, err = s.selectFromShutterstock(pinID)
	if err != nil {
		return pin, err
	}

	if pin.Pin.ImageID != "" {
		return pin, err
	}

	return s.selectFromImages(pinID)
}

func (s *ServerType) selectFromShutterstock(pinID string) (pin Pin, err error) {

	var rows *sql.Rows

	rows, err = s.db.Query("SELECT * FROM shutterstock WHERE image_id=$1", pinID)
	if err != nil {
		s.l.Fatal(err.Error())
	}

	defer rows.Close()

	for rows.Next() {
		errScan := rows.Scan(
			&pin.Pin.ImageID,
			&pin.Pin.Title,
			&pin.Pin.Link,
			&pin.Pin.ImageSrc)
		if err != nil {
			s.l.Fatal(errScan.Error())
		}
	}

	return pin, err
}

func (s *ServerType) selectFromImages(pinID string) (pin Pin, err error) {

	var description, contentURL, imageLink string

	row := s.db.QueryRow("SELECT description, content_url, image_link FROM images WHERE id=$1", pinID)
	switch err = row.Scan(&description, &contentURL, &imageLink); err {
	case sql.ErrNoRows:
		return pin, err
	case nil:
		return Pin{
			Pin: DataType{
				ImageID:  pinID,
				Title:    description,
				Link:     imageLink,
				ImageSrc: contentURL,
			},
		}, err
	default:
		return pin, err
	}
}

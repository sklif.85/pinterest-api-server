BIN=pinterest-api-server

GOOS=linux
GOARCH=amd64
CGO_ENABLED=0
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
GOFLAGS=-ldflags '-w -s'

DOCKER=docker
DOCKERBUILD=$(DOCKER) build
DOCKERRUN=$(DOCKER) run
DOCKERPUSH=$(DOCKER) push

GODEP=dep
NEWDEP=$(GODEP) ensure

AWS=alex
AWSECR=848984447616.dkr.ecr.us-east-1.amazonaws.com/$(BIN)
AWSLOGIN=aws ecr --profile alex get-login --no-include-email --region us-east-1 | sed 's|https://||'

all: go-build docker-build aws-login docker-push clean

go-build:
	@echo "Golang build executable..."
	$(NEWDEP)
	GOOS=$(GOOS) GOARCH=$(GOARCH) CGO_ENABLED=$(CGO_ENABLED) $(GOBUILD) -o $(BIN) $(GOFLAGS) \
	main.go \
	config.go \
	health_check.go \
	image.go \
	middleware.go \
	new_logger.go \
	new_server.go \
	locations.go \
	psql.go

aws-login:
	`eval $(AWSLOGIN)`

docker-build:
	@echo "Docker build service..."
	$(DOCKERBUILD) --no-cache -t $(BIN) .

docker-push:
	@echo "Push Docker image to AWS ECR..."
	docker tag $(BIN):latest $(AWSECR):latest
	eval $(aws ecr --profile alex get-login --no-include-email --region us-east-1 | sed 's|https://||')
	docker push $(AWSECR):latest

run:
	@echo "Docker run service..."
	$(DOCKERRUN) \
	--rm \
	-dt \
	-p 8001:8001 \
	--name=$(BIN) \
	-v $(pwd)/app_logs:/app_logs
	--net my_app
	-e PROD='1'
	$(BIN)

clean:
	@echo "Clean"
	$(GOCLEAN)
	rm -f $(BINARY)
	#docker rmi $(docker images -q)
	#docker rm $(docker ps -q -f status=exited)



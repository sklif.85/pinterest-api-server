package main

import (
	"net/http"
	"time"
	"encoding/json"
)

type HealthCheckType struct {
	Service  string
	CurrDate string
	Version  string
}

func (s *ServerType) HealthCheck(w http.ResponseWriter, r *http.Request) {
	data := HealthCheckType{
		Service:  "pinterest-api-service",
		CurrDate: time.Now().Format("2006-01-02 15:04"),
		Version:  "1.0",
	}

	payload, err := json.Marshal(data)
	if err != nil {
		s.l.Error("Marshal")
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(payload)
	s.l.Info("get health-check")
}

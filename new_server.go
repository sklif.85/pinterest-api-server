package main

import (
	"database/sql"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"go.uber.org/zap"

	"github.com/gorilla/handlers"
	"net/http"
	"time"
)

type CORS struct {
	headers handlers.CORSOption
	allow   handlers.CORSOption
	methods handlers.CORSOption
}

type ServerType struct {
	l  *zap.Logger
	c  *ConfigPSQL
	r  *mux.Router
	s  *http.Server
	db *sql.DB
}

func Server(zapLog *zap.Logger, confPath string) *ServerType {
	server := &ServerType{}

	server.l = zapLog
	server.c = server.LoadConfig(confPath)
	server.r = mux.NewRouter()
	server.db = server.ConnectPSQL()

	server.s = &http.Server{
		Handler:      server.handlersCORS(),
		Addr:         ":8080",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	server.l.Info("New server create")

	return server
}

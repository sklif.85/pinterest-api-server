package main

import (
	"github.com/gorilla/handlers"
	"net/http"
	"html/template"
)

func (s *ServerType) Router() {

	s.r.HandleFunc("/healthcheck", s.HealthCheck).Methods(http.MethodGet)

	s.r.HandleFunc("/getpindata/{pinid}", s.Image).Methods(http.MethodGet)

	s.r.HandleFunc("/locations", s.Locations).Methods(http.MethodGet)

	s.r.HandleFunc("/map", locationsMap).Methods(http.MethodGet)

	s.l.Info("Create routes")
}

func (s *ServerType) handlersCORS() http.Handler {
	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type"})
	origins := handlers.AllowedOrigins([]string{"*"})
	methods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})
	return handlers.CORS(headers, origins, methods)(s.r)
}


func locationsMap(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("locations.html") //setp 1
	t.Execute(w, "Hello World!") //step 2
}
package main

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

type ConfigPSQL struct {
	DB        string
	User      string
	Password  string
	Hostlocal string
	Hostprod  string
	Port      string
}

func (s *ServerType) LoadConfig(confPath string) *ConfigPSQL {
	confPSQL := ConfigPSQL{}
	data, errReadFile := ioutil.ReadFile(confPath)
	if errReadFile != nil {
		s.l.Error(errReadFile.Error())
		os.Exit(1)
	}

	errYaml := yaml.Unmarshal(data, &confPSQL)
	if errYaml != nil {
		s.l.Error(errYaml.Error())
		os.Exit(1)
	}

	s.l.Info("Load config")

	return &confPSQL
}

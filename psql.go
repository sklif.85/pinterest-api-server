package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

func (s *ServerType) GetHost() string {
	//prod := os.Getenv("PROD")
	//if strings.Compare(prod, "0") == 1 {
	//	return s.c.Hostprod
	//}
	//return s.c.Hostlocal

	return "postgres"
	//
	//return "18.210.250.132"
}

func (s *ServerType) ConnectPSQL() *sql.DB {

	addr := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
		s.GetHost(), s.c.User, s.c.Password, s.c.DB)

	s.l.Info(addr)

	dbConn, errConn := sql.Open("postgres", addr)
	if errConn != nil {
		s.l.Error(fmt.Sprintf("Failed connect to PSQL...%s", addr))
	}
	s.l.Info(fmt.Sprintf("Success connect to PSQL...%s", addr))

	errPing := dbConn.Ping()
	if errPing != nil {
		s.l.Fatal(errPing.Error())
	}

	return dbConn
}

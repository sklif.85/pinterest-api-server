package main

import (
	"go.uber.org/zap"
	"fmt"
	"time"
	"os"
)

func Logger(logPath string, logFile string) *zap.Logger {

	filename := fmt.Sprintf("%s/%s_%s.log", logPath, logFile, time.Now().Format("2006-01-02_15:04"))
	_, err := os.Create(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Creation of log file errored!")
		os.Exit(1)
	}

	conf := zap.NewProductionConfig()
	conf.OutputPaths = []string{
		filename,
	}

	zapLog, err := conf.Build()
	if err != nil {
		fmt.Println(err)
	}

	defer zapLog.Sync()

	zapLog.Info("Logger successful initialization")

	return zapLog
}

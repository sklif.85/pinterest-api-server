package main

import (
	"net/http"
	"encoding/json"
	"strings"
)

type Locations struct {
	Country string `json:"country"`
	Count   int    `json:"count"`
}

func (s *ServerType) Locations(w http.ResponseWriter, r *http.Request) {

	data := s.GetLocations()

	res := parseData(data)

	b, err := json.Marshal(res)
	if err != nil {
		s.l.Error(err.Error())
	}

	_, err1 := w.Write(b)
	if err1 != nil {
		s.l.Error(err.Error())
	}
}

func (s *ServerType) GetLocations() []Locations {


	rows, err := s.db.Query("SELECT country, count(country) FROM location WHERE country != $1 GROUP BY country", "None")
	if err != nil {
		s.l.Fatal(err.Error())
	}

	defer rows.Close()

	locations := []Locations{}
	//keys := []string{}
	//mapLoc := map[int]Locations{}

	for rows.Next() {
		l := Locations{}
		errScan := rows.Scan(
			&l.Country,
			&l.Count)
		if err != nil {
			s.l.Fatal(errScan.Error())
		}
		locations = append(locations, l)
	}


	return locations
}

func parseData(data []Locations) [][]interface{} {
	res := [][]interface{}{}
	for _, location := range data {
		r := []interface{}{strings.Trim(location.Country, " "), location.Count}
		res = append(res, r)
	}
	return res
}

